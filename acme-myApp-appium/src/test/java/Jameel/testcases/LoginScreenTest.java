package Jameel.testcases;

import org.apache.commons.io.filefilter.FalseFileFilter;
import org.testng.Assert;
import org.testng.annotations.Test;

import Actions.Action;

import Jameel.screens.*;

import io.appium.java_client.MobileElement;

public class LoginScreenTest extends Action {
	
	StartPage startPage; 
	
	
	@Test(priority=1,enabled = true)
	public void LginSuccessfully() throws InterruptedException {
		startPage = new StartPage();
	
	click(startPage.PhoneNumber);
	EnterMobileNumber("555222777");
	click(startPage.NextButton);
	click(startPage.Password);
	EnterMobileNumber("Samar@123456");
	backButton();
	click(startPage.StartButton);
	click(startPage.DiscardButton);

       final String text =startPage.Search.getAttribute("content-desc").toString();
	    Assert.assertTrue(text.contains("Search"));		
	}

	@Test(priority=2,enabled = true)
	public void RegisterNumber() throws InterruptedException {
		startPage = new StartPage();
	
	click(startPage.PhoneNumber);
	EnterMobileNumber("555333666");
	click(startPage.NextButton);


       final String text =startPage.RegisterMessage.getAttribute("content-desc").toString();
	    Assert.assertTrue(text.contains("Register"));		
	}

	@Test(priority=3,enabled = true)
	public void incorrectPassword() throws InterruptedException {
		startPage = new StartPage();
	
		click(startPage.PhoneNumber);
		EnterMobileNumber("555222777");
		click(startPage.NextButton);
		click(startPage.Password);
		EnterMobileNumber("Samar@123");
		backButton();
		click(startPage.StartButton);
       final String text =startPage.IncorrectPasswordMessage.getAttribute("content-desc").toString();
	    Assert.assertTrue(text.contains("Password incorrect"));		
	}

	@Test(priority=4,enabled = true)
	public void forgetPassword() throws InterruptedException {
		startPage = new StartPage();
	
		click(startPage.PhoneNumber);
		EnterMobileNumber("555222777");
		click(startPage.NextButton);
		click(startPage.ForgetPassword);
       final String text =startPage.ResetPassMessage.getAttribute("content-desc").toString();
	    Assert.assertTrue(text.contains("Reset password"));		
	}


	
}
