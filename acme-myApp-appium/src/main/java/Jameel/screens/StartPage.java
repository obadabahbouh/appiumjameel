package Jameel.screens;
import java.util.List;

import Jameel.base.Base;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class StartPage extends Base{
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (xpath ="//android.view.View[@content-desc=\"Start now with us\nA whole world of personal care services and enjoy the highest level of privacy and security.\nOr you can sign in with\"]/android.widget.ImageView[2]")
	public MobileElement PhoneNumber;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Next")
	public MobileElement NextButton;

	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (xpath ="//android.widget.ImageView[@content-desc=\"Start now with us\nA whole world of personal care services and enjoy the highest level of privacy and security.\"]/android.widget.EditText")
	public MobileElement Password;

	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Start")
	public MobileElement StartButton;
	
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Discard")
	public MobileElement DiscardButton;
	  
	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Search")
	public MobileElement Search;

	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Register\nA verification code was sent via SMS to the number 00971555333666 please enter it")
	public MobileElement RegisterMessage;

	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Start now with us\nA whole world of personal care services and enjoy the highest level of privacy and security.\nPassword incorrect")
	public MobileElement IncorrectPasswordMessage;

	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Forget password?")
	public MobileElement ForgetPassword;

	@iOSXCUITFindBy (accessibility = "")
	@AndroidFindBy (accessibility ="Reset password\nA verification code was sent via SMS to the number 00971555222777 please enter it")
	public MobileElement ResetPassMessage;
}
