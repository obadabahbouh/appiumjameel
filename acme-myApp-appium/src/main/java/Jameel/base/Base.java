package Jameel.base;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.FieldDecorator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class Base {
  protected static AndroidDriver<MobileElement> driver;
  
  public Base() {
    PageFactory.initElements((FieldDecorator)new AppiumFieldDecorator((SearchContext)driver), this);
  }
  
  @AfterClass
  public void restartApp() {
    driver.resetApp();
  }
  
  @BeforeClass
  public void navigateTo() throws InterruptedException {
    driver.launchApp();
  }
  
  @BeforeSuite
  public void beforeMethod() throws MalformedURLException {
    DesiredCapabilities caps = new DesiredCapabilities();
    URL appiumURL = new URL("http://127.0.0.1:4723/wd/hub");
    driver = new AndroidDriver(appiumURL, (Capabilities)caps);
    driver.manage().timeouts().implicitlyWait(32L, TimeUnit.SECONDS);
  }
  
  @AfterSuite
  public void afterMethod() {
    driver.quit();
  }
}
